<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@lang('welcome.welcome')</title>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://usrz.github.io/bootstrap-languages/languages.min.css"/>
    <!-- Styles -->
    <style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        height: 100vh;
        margin: 0;
    }
    .full-height {
        height: 100vh;
    }
    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }
    .position-ref {
        position: relative;
    }
    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }
    .content {
        text-align: center;
    }
    .title {
        font-size: 84px;
    }
    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }
    .links > a:hover{
        color: red;
    }
    .m-b-md {
        margin-bottom: 30px;
    }
</style>
</head>
<body>
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                @lang('welcome.welcome')
            </div>
            <div class="links">
                <a href="/one_page">@lang('welcome.one_page')</a>
                <a href="/one_page">@lang('welcome.one_page')</a>
            </div>
            <div class="links">
                <a href="{{ url('lang', ['es']) }}">Es</a>
                <a href="{{ url('lang', ['en']) }}">En</a>
            </div>
        </div>
    </div>
</body>
</html>
